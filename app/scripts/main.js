'use strict';

// ready
$(document).ready(function() {

    // preload img
    function preloadImages() {
        for (var i = 0; i < arguments.length; i++) {
            new Image().src = arguments[i];
        }
    }
    preloadImages(
        "../images/catalog-back1.jpg",
        "../images/catalog-back.jpg"
    );
    // preload img


    // adaptive menu
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
    });
    // adaptive menu

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone

    // slider (hor slider)
    (function () {
        var $frame = $('.slider-sh');
        var $wrap  = $frame.parent();
        $frame.sly({
            horizontal: 1,
            itemNav: 'basic',
            smart: 1,
            activateOn: 'click',
            mouseDragging: 0,
            touchDragging: 1,
            releaseSwing: 1,
            startAt: 0,
            scrollBar: $wrap.find('.scrollbar'),
            scrollBy: 1,
            activatePageOn: 'click',
            speed: 300,
            elasticBounds: 1,
            easing: 'easeOutExpo',
            dragHandle: 1,
            dynamicHandle: 1,
            clickBar: 1,

            // Buttons
            prev: $wrap.find('.prev'),
            next: $wrap.find('.next')
        });
    }());
    // slider {slick-carousel}
    $('.slider').slick({
        dots: true
        // autoplay: true,
        // autoplaySpeed: 2000
    });
    $('.carousel').slick({});
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        // autoplay: true,
        afterChange: function (slickSlider, i) {
            $('.slider-nav .slick-slide').removeClass('slick-active');
            $('.slider-nav .slick-slide').eq(i).addClass('slick-active');
        }
    });
    $('.slider-nav .slick-slide').eq(0).addClass('slick-active');
    $('.slider-nav').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false
    });
    $('.slider-nav').on('mouseenter', '.slick-slide', function (e) {
        var $currTarget = $(e.currentTarget),
            index = $currTarget.data('slick-index'),
            slickObj = $('.slider-for').slick('getSlick');
        slickObj.slickGoTo(index);
    });
    // slider


    //.menu-open--js
    $('.menu-open--js').click(function () {
        $('body').css('overflow', 'hidden');
       $(this).next().addClass('active');
        return false;
    });
    $('.menu-close--js').click(function () {
        $('body').css('overflow', 'auto');
       $(this).closest('.menu').removeClass('active');
        return false;
    });
    $('.showall--js').click(function () {
       $(this).nextAll().show(100).css("display", "inline-block");
        $(this).remove();
    });
    //.menu-open--js

    // select {select2}
    //$('select').select2({});
    // select

    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs


    //catalog backgrounds
    $('.catalog__item--js').hover(
        function () {
            var catalogBackground = $(this).attr('data-back');
            $('.page-catalog').css('background-image', 'url(' + catalogBackground + ')');
        },
        function () {
            $('.page-catalog').css('background-image', 'url(images/catalog-back.jpg)');
        }
    );
    //catalog backgrounds

    // popup {magnific-popup}
    $('.popup-modal').magnificPopup({
        removalDelay: 500, //delay removal by X to allow out-animation
        callbacks: {
            beforeOpen: function() {
                this.st.mainClass = this.st.el.attr('data-effect');
            },
            change: function() {
                setInterval (function(){
                    $('.slider-nav').slick('setPosition');
                    $(".slider-for").slick("setPosition");
                }, 1);

            }
        },
        midClick: true
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });
    // popup

    //custom scroll
    $('.wherebuy-contacts--inner').mCustomScrollbar();
    //custom scroll

});
// ready

function initMap() {
    var markers = [];
    var locations = [
        ['<p class="text__lg">ул.Хабаровская, д.6</p><p class="text__xs">график работы:</p><span>10<sup>00</sup>-20<sup>00</sup></span><br><span class="text__xs">без выходных</span>',59.91916157, 30.3251195],
        ['<p class="text__lg">ул.Хабаровская, д.6</p><p class="text__xs">график работы:</p><span>10<sup>00</sup>-20<sup>00</sup></span><br><span class="text__xs">без выходных</span>', 59.91756978, 30.31812429],
        ['<p class="text__lg">ул.Хабаровская, д.6</p><p class="text__xs">график работы:</p><span>10<sup>00</sup>-20<sup>00</sup></span><br><span class="text__xs">без выходных</span>', 59.92049517, 30.33250093],
        ['<p class="text__lg">ул.Хабаровская, д.6</p><p class="text__xs">график работы:</p><span>10<sup>00</sup>-20<sup>00</sup></span><br><span class="text__xs">без выходных</span>', 59.91701049, 30.3276515],
        ['<p class="text__lg">ул.Хабаровская, д.6</p><p class="text__xs">график работы:</p><span>10<sup>00</sup>-20<sup>00</sup></span><br><span class="text__xs">без выходных</span>', 59.92032309, 30.32258749]
    ];

    var mapOptions = {
        center: new google.maps.LatLng(59.91916157, 30.3251195),
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        scrollwheel: false
    };
    map = new google.maps.Map(document.getElementById("map"), mapOptions);

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map,
            icon: "../images/info-maps.png"
        });
        markers.push(marker);
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                //console.log(activeIcon);
                for (var j = 0; j < markers.length; j++) {
                    markers[j].setIcon('../images/info-maps.png');
                }
                infowindow.setContent(locations[i][0]);
                this.setIcon('../images/info-maps-ac.png');
                infowindow.open(map, marker);
            }
        })(marker, i));
    }


}

